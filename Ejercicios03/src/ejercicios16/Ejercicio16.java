package ejercicios16;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		System.out.println("Introduce una hora en formato h:m:s");
		String horaCompleta = input.nextLine();

		// extraer en tipos enteros las horas, minutos, segundos
		int primerosDosPuntos = horaCompleta.indexOf(":");
		int segundosDosPuntos = horaCompleta.lastIndexOf(":");

		// dividir
		String horasString = horaCompleta.substring(0, primerosDosPuntos);
		String minutosString = horaCompleta.substring(primerosDosPuntos + 1, segundosDosPuntos);
		String segundosString = horaCompleta.substring(segundosDosPuntos + 1);

		// traspaso a int
		int horasEntero = Integer.parseInt(horasString);
		int minutosEntero = Integer.parseInt(minutosString);
		int segundosEntero = Integer.parseInt(segundosString);

		// creo comprobaciones
		boolean horasValidas = horasEntero >= 0 && horasEntero <= 23;
		boolean minutosValidas = minutosEntero >= 0 && minutosEntero <= 59;
		boolean segundosValidas = segundosEntero >= 0 && segundosEntero <= 59;

		// compruebo valores
		if (horasValidas && minutosValidas && segundosValidas) {
			System.out.println("El formato de horas es correcto");
		} else {
			System.out.println("El formato de horas no es correcto");
		}

		System.out.println("Introduce la fecha en formato d/m/a");
		String fechaCompleta = input.nextLine();

		int posicionPrimeraBarra = fechaCompleta.indexOf('/');
		int posicionSegundaBarra = fechaCompleta.lastIndexOf('/');

		// divido y paso a int en una sola acci�n
		int dias = Integer.parseInt(fechaCompleta.substring(0, posicionPrimeraBarra));
		int meses = Integer.parseInt(fechaCompleta.substring(posicionPrimeraBarra + 1, posicionSegundaBarra));
		int annos = Integer.parseInt(fechaCompleta.substring(posicionSegundaBarra + 1));

		// creo comprobaciones
		boolean mesesValidos = meses >= 0 && meses <= 12;
		boolean annosValidos = annos >= 0;

		// comprobaci�n dias
		boolean diasValidos = false;
		if ((meses == 1 || meses == 3 || meses == 5 || meses == 7 || meses == 8 || meses == 10 || meses == 12)
				&& dias > 0 && dias <= 31) {
			diasValidos = true;
		} else if ((meses == 4 || meses == 6 || meses == 9 || meses == 11) && dias >= 0 && dias <= 30) {
			diasValidos = true;
		} else if (meses == 2 && dias > 0 && dias <= 28) {
			diasValidos = true;
		} else if (meses == 2 && dias > 0 && dias <= 29 && annos % 4 == 0) {
			diasValidos = true;
		}

		// compruebo valores
		if (diasValidos && mesesValidos && annosValidos) {
			System.out.println("Formato fecha correcta");
		} else {
			System.out.println("Formato de fecha no correcta");
		}

		input.close();
	}

}
