package Ejercicios07;

import java.util.Scanner;

public class Ejercicio07 {

	public static void main(String[] args) {
		Scanner lector =new Scanner(System.in);
		
		System.out.println("Introduce un numero para ver el caracter al que corresponde");
		int numero=  lector.nextInt();
		
		System.out.println((char)numero);
		
		lector.close();

	}

}
