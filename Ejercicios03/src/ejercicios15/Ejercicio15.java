package ejercicios15;

import java.util.Scanner;

public class Ejercicio15 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		//pedimos hora
		System.out.println("Introduce una hora en formato horas:minutos:segundos");
		String horaCompleta = input.nextLine();

		// obtener las posiciones de los :
		// 15:02:12
		int posicionPrimerosDosPuntos = horaCompleta.indexOf(":");
		int posicionSegundosDosPuntos = horaCompleta.lastIndexOf(":");
		// otra posibilidad
		// int
		// posicionSegundosDosPuntos=horaCompleta.indexOf(":",posicionPrimerosDosPuntos)

		// obtengo las subcadenas
		String horasCadena = horaCompleta.substring(0, posicionPrimerosDosPuntos);
		String minutosCadena = horaCompleta.substring(posicionPrimerosDosPuntos + 1, posicionSegundosDosPuntos);
		String segundosCadena = horaCompleta.substring(posicionSegundosDosPuntos + 1);

		// convierto cada String a int
		int horas = Integer.parseInt(horasCadena);
		int minutos = Integer.parseInt(minutosCadena);
		int segundos = Integer.parseInt(segundosCadena);

		//comprobar validez
		if (horas >= 0 && horas <= 23 && minutos >= 0 && minutos <= 59 && segundos >= 0 && segundos <= 59) {
			System.out.println("El formato de la hora es correcta");
		} else {
			System.out.println("El formato de la hora es incorrecto");
		}

		//pedimos fecha
		System.out.println("Introduce una fecha en formato dia/mes/a�o");
		String fechaCompleta = input.nextLine();

		//obtener las posiciones de la /
		int posicionPrimeraBarra = fechaCompleta.indexOf('/');
		int posicionSegundaBarra = fechaCompleta.lastIndexOf('/');

		//obtengo las subcadenas
		String cadenaDia = fechaCompleta.substring(0, posicionPrimeraBarra);
		String cadenaMes = fechaCompleta.substring(posicionPrimeraBarra + 1, posicionSegundaBarra);
		String cadenaAnno = fechaCompleta.substring(posicionSegundaBarra + 1);

		//convierto cada String a int
		int dia = Integer.parseInt(cadenaDia);
		int mes = Integer.parseInt(cadenaMes);
		int anno = Integer.parseInt(cadenaAnno);

		//compruebo validez
		if (dia >= 1 && dia <= 30) {
			if (mes >= 1 && mes <= 12) {
				if (anno >= 1) {
					System.out.println("La fecha es correcta");
				} else {
					System.out.println("El a�o es incorrecto");
				}
			} else {
				System.out.println("El mes es incorrecto");
			}
		} else {
			System.out.println("El dia es incorrecto");
		}

		input.close();
	}

}
