package Ejercicio08;

import java.util.Scanner;

public class ejercicio08 {

	public static void main(String[] args) {
		Scanner lector = new Scanner(System.in);
		
		System.out.println("Introduce el dividendo (entero)");
		int dividendo= lector.nextInt();
		
		System.out.println("Introduce el divisor (entero)");
		int divisor=lector.nextInt();
		
		if (divisor !=0) 	{
			System.out.println(dividendo/divisor);
		
		} else {
			System.out.println("No se puede dividir por 0");
		}
		
		
		lector.close();

	}

}
